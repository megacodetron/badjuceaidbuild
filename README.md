# Project to demonstrate JUCE build issues

This reproduces a setup for JUCE 8.0.0 which fails to build on linux under clang 18.1.8

## Requirements to reproduce
  - cmake
  - clang-18
  - git
  - set the environment variable `CPM_SOURCE_CACHE` where Cmake packages will be cached

## Linux version

```
> uname -a
Linux Box 6.5.0-41-generic #41-Ubuntu SMP PREEMPT_DYNAMIC Mon May 20 15:55:15 UTC 2024 x86_64 x86_64 x86_64 GNU/Linux```

## Clang version
```
> clang-18 --version
Ubuntu clang version 18.1.8 (++20240615103833+3b5b5c1ec4a3-1~exp1~20240615223845.150)
Target: x86_64-pc-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
```

## Steps to reproduce

Execute the shell script `runThis.sh

The output of a failed run can be found in `failedOutput`