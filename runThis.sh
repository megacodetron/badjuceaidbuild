rm -rf build
cmake -S . -GNinja -B build -DCMAKE_CXX_COMPILER=clang++-18 -DCMAKE_C_COMPILER=clang-18
cd build
ninja
